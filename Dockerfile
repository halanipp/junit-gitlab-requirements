FROM node:alpine

WORKDIR /app

COPY src/ .

RUN apk --no-cache add curl jq \
    && ln -s /app/index.js /usr/bin/junit-2-gitlab-requirements
