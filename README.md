# junit-2-gitlab-requirements

Tooling that maps JUnit test reports (`junit.xml`) into GitLab Requirements test reports (`requirements.json`).

With this enabled, whenever your pipeline artifacts JUnit test reports (via `artifacts:reports:junit`), it will also automatically artifact a GitLab Requirements report.

## Usage

1. Import `JUnit-2-GitLab-Requirements.gitlab-ci.yml` into your own GitLab project's pipeline, e.g.:

   ```yml
   include:
     - remote: https://gitlab.com/halanipp/junit-2-gitlab-requirements/-/raw/main/JUnit-2-GitLab-Requirements.gitlab-ci.yml
   ```

2. Ensure the `junit-2-gitlab-requirements` stage has an environment variable `GITLAB_TOKEN` defined. (This can of course be inherited).

   The token must have at least `read_api` scope within your GitLab project (which allows it to fetch your project's requirements).

   **NOTE**: the token must be a _personal_ access token (not _project_), due to a [GitLab bug](https://gitlab.com/gitlab-org/gitlab/-/issues/255354).

3. Done.

## Example

See [example](example/) directory.
